# RandomPathWriter
RandomPathWriter is a simple Python script that recurses through all directories below a specified `root`. In each directory it will write between 1 and 3 files with a random filename and a random size between a min and max. The only use case is... well, I don't know. I just thought it would be fun to write. Also you could use it to fill a machines file system in a super annoying way as it would be difficult to write a script to delete all of the files that were created, lol.

## Usage
The script can be executed from the command line using the following command:

```sh
python path_generator.py write [root] [MIN_SIZE] [MAX_SIZE]
```

The script will recursively visit each directory in the tree below the specified root directory and write 1-3 random files with a random filename and size between MIN_SIZE and MAX_SIZE bytes in each directory. Once every directory has a random file in it, the script will halt.

`MIN_SIZE` and `MAX_SIZE` are optional arguments. If they are not specified, the script will use a default value of 1kB for `MIN_SIZE` and 10MB for `MAX_SIZE`.

## Cleanup
The script keeps a log of all the files it creates in a file called `written_files.txt`. This file is located next to the `path_generator.py` script. If you want to delete all of the files that were created, you can use the following command:

```sh
python path_generator.py clean
```

Additionally a `.log` file is kept in the root of the repo. This log is written to whenever a file is created or deleted. The log can be used as another way to track down any rouge files that were not deleted.

Don't fret I've run it on my own machine, filled my drive, then run the cleanup and everything was removed as expected 😁

## Requirements
Install requirements using the following command:

```sh
pip install -r requirements.txt
```

## Enjoy! 😉
