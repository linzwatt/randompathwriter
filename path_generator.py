import click
import random
import string
import logging
import traceback

from tqdm import tqdm
from pathlib import Path
from typing import Union, Generator, List


COMMON_EXTENSIONS = None
COMMON_EXTENSIONS_PATH = "./common_extensions.txt"
WRITTEN_FILES_PATH = "./written_files.txt"
LOGFILE_PATH = "./path_generator.log"


def load_extensions(path: Union[str, Path] = COMMON_EXTENSIONS_PATH) -> List[str]:
    path = clean_path(path)
    common_extensions: List[str] = []

    logging.info(f"Loading extensions from {path}...")

    if path.exists():
        common_extensions = path.read_text().splitlines()

        # ensure each extension is lowercase, and starts with a dot
        for i, ext in enumerate(common_extensions):
            ext = ext.strip().lower()
            if not ext.startswith("."):
                ext = f".{ext}"
            common_extensions[i] = ext

        # remove duplicates and sort
        common_extensions = list(set(common_extensions))
        common_extensions.sort()

        # write cleaned extensions back to file
        path.write_text("\n".join(common_extensions))

    logging.info(f"Loaded {len(common_extensions):,d} extensions.")

    return common_extensions


def random_path_generator(root: Union[str, Path]) -> Generator[Path, None, None]:
    root = clean_path(root)
    yield root
    for path in root.rglob("*"):
        if path.is_dir():
            yield path


def random_filename() -> str:
    # load common extensions if not already loaded
    global COMMON_EXTENSIONS
    if not COMMON_EXTENSIONS:
        COMMON_EXTENSIONS = load_extensions()

    # define the alphabet for generating filenames
    safe_start_end_alphabet = string.ascii_letters
    fn_alphabet = string.ascii_letters + string.digits + " _-.~()"

    # always start and end with a letter, in case filesystem complains
    start = random.choice(safe_start_end_alphabet)
    end = random.choice(safe_start_end_alphabet)

    # generate a random filename and extension
    filename = "".join(random.choice(fn_alphabet) for _ in range(random.randint(1, 35)))
    extension = random.choice(COMMON_EXTENSIONS)

    return f"{start}{filename}{end}{extension}"


def random_bytes(n_bytes: int) -> bytes:
    return random.randbytes(n_bytes)


def write_bytes(path: Union[str, Path], contents: bytes) -> int:
    # prepare the paths
    written_files_path = clean_path(WRITTEN_FILES_PATH)
    path = clean_path(path)

    # save the written filename so we can clean it up later
    # do this before actually writing the file, in case of a partial write or other weirdness
    with open(written_files_path, "at") as f:
        f.write(f"{path}\n")

    try:
        bytes_written = path.write_bytes(contents)

    except Exception as e:
        logging.error(f"Error writing {path}: {e}")
        logging.error(traceback.format_exc())
        return 0

    logging.debug(f"Wrote {bytes_written:,d} bytes to {path}.")
    tqdm.write(f"Wrote {bytes_written:,d} bytes to {path}.")
    return bytes_written


def files_generator(root: Union[str, Path]) -> Generator[Path, None, None]:
    root = clean_path(root)
    for path in root.rglob("*"):
        if path.is_file():
            yield path


def clean_written_files():
    written_files_path = clean_path(WRITTEN_FILES_PATH)
    written_files = written_files_path.read_text().splitlines()

    for file in tqdm(
        written_files, desc="Cleaning files", unit="files", smoothing=0.01, position=1
    ):
        file = clean_path(file)
        if file.is_file() and file.exists():
            logging.debug(f"Deleting {file}...")
            tqdm.write(f"Deleting {file}...")
            file.unlink()

    if not written_files:
        logging.info("No files to delete.")

    # clear written files list
    written_files_path.write_text("")


def clean_path(path: Union[str, Path]) -> Path:
    return Path(path).expanduser().resolve()


def init_logging(logfile_path=LOGFILE_PATH):
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()
    handler = logging.FileHandler(logfile_path)
    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - `%(message)s` - %(pathname)s:%(funcName)s:%(lineno)d"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


@click.group()
def cli():
    pass


@cli.command()
@click.argument("root", type=click.Path(dir_okay=True), default=None)
@click.argument("min_size", type=click.IntRange(min=1, max=100_000_000), default=1_000)
@click.argument(
    "max_size", type=click.IntRange(min=1, max=100_000_000), default=10_000_000
)
def write(root: Union[str, Path], min_size: int, max_size: int):
    assert root, f"root must be specified, got `{root}`."

    print("root", root)

    root = clean_path(root)
    assert root.exists(), f"{root} does not exist."
    assert root.is_dir(), f"{root} is not a directory."

    assert min_size > 0, f"min_size must be greater than 0, got `{min_size}`."
    assert max_size > 0, f"max_size must be greater than 0, got `{max_size}`."
    assert (
        max_size <= 100_000_000
    ), f"max_size must be less than 100MB, got `{max_size}`."
    assert (
        max_size > min_size
    ), f"max_size must be greater than min_size, got `{max_size}` and `{min_size}`."

    try:
        # iterate through all paths at the root and deeper in the directory tree
        for path in tqdm(
            random_path_generator(root),
            desc="Writing files",
            unit="paths",
            smoothing=0.01,
            position=1,
        ):
            # at each path, create at least 1 file, up to a few
            for _ in range(random.randint(1, 3)):
                filepath = path / random_filename()
                contents = random_bytes(random.randint(int(min_size), int(max_size)))
                write_bytes(filepath, contents)

    except click.Abort:
        logging.info("Exiting.")


@cli.command()
def clean():
    clean_written_files()


if __name__ == "__main__":
    init_logging()
    cli()
